+++
title = "About"
date = "2022-06-28"
author = "Nick"
+++

# Hi there and welcome to my blog!

My name is Nick and I am currently living in southwestern Florida. I am currently studying computer science in university, but I have always had an interest in public transport. Ever since I rode on the subway for the first time, that interest turned into a love, and here I am today - writing a blog about public transit.

On this blog, I plan on reviewing bus routes, train lines, and various stations  throughout North America and beyond - however, the main focus will be on the southeastern US and Florida. In the future, I hope to expand and do a few more extensive transit adventures.

That’s all for now… enjoy!
