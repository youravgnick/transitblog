---
title: "Norfolk's Light Rail - The Tide"
date: 2022-07-26T18:56:04Z
author: "Nick"
cover: /img/tide-streetcar.jpg
description: "The least used light rail line in America isn't too bad."
---

Here we go with my first out of state post, Hampton Roads Transit's Tide light rail line. The least used in the US - a singular line connects downtown Norfolk with various stations at park and rides, a baseball stadium, Norfolk State University, and the Amtrak station. And that is all that comprises Norfolk's rail network, and I believe it could be a lot more useful if it was expanded even just slightly.

{{< figure src="/img/tidelr-map.png" caption="Map of Norfolk's Light Rail line" >}}

While currently a few stations are park-and-rides, there is hope for transit oriented development along these stations - it *is* the original intent after all. Hampton Roads Transit is looking into potential expansion of the line, and at one point, it hoped to extend a line to Virginia Beach. This was approved by voters and the mayor in 2012, and the city subsequently purchased the part of Norfolk Southern's railroad within its jurisdiction.

In 2016, however, Virginia Beach voters voted against funding the rail line's extension, effectively halting the project indefinetly. And while I could talk about potential extentions for a long time, let's talk about the rail line that does already exist. 

With 10 minute headways during morning and evening rush hours, 15 minute headways other times during weekdays, and 30 minute headways after 10 PM, the system is pretty decent when it comes to timing. Even on weekends, service is every 15 minutes for most of the day. 

When it comes to ridership, though, it is notoriously low. On weekdays, the system gets about 2200 passengers, and annual ridership is only 620,800 as of 2021. I was practically the only person on the train when I rode it, and that was in the afternoon. This could be attributed to the Hampton Roads area being mostly suburban, and with more transit oriented development coming soon, ridership may improve in the future.

All in all, I'd give this line a 6.5/10. It could be a lot better with stations that it serves, but it's headways, fare price, and cleanliness make up for it. 
