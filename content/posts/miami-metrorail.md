---
title: "Miami Dade Transit - Metrorail"
date: 2022-06-30T17:29:12Z
author: "Nick"
cover: /img/metrorail-gov-center.jpg
description: "Miami's rapid transit has potential to be a lot more."
---

Miami - the magic city, home of Little Havana, South Beach, and the southernmost rail station in the United States. While I won't be reviewing Miami's Amtrak or Brightline stations today, I will be reviewing Miami's rapid transit lines - the Metrorail.

The Metrorail technically has two lines, however it effectively functions as one with a split happening near the northern termini, the Orange line going to MIA airport and the Green line going to Hialeah and the Tri-Rail transfer station. (See map below)

{{< figure src="/img/metrorail-map.png" caption="Map of Miami's Metrorail" >}}

The system is quite punctual, however frequency could be *a lot* better. Weekends have a dismal 30 minute headway between trains on the same line, with a 15 minute headway between trains on both lines. If you miss your train to the airport, good luck!

The Metrorail, combined with the free Metromover downtown (which will be featured in another post), is an effective way for at least some people to commute **downtown**, however it doesn't go everywhere that people visiting Miami would want to go, such as the beach, the zoo, and other major attractions - and the Metrobus network isn't the most reliable either.

The horrendous weekend headways paired with the limited bus network make the Miami area one that is quite reliant on cars, which is sad because the potential for a lot more is there.

The expansion of the Metrorail and a potential extension of the free Metromover to Miami Beach would help locals *and* tourists move around the city a lot easier, as well as the added benefit of less people on the extremely congested Miami area freeways and roads.

An easier option to improve Miami-area transit in the meantime would be to simply improve headways on both the Metrorail and on Metrobuses. Hour long waits between crosstown buses is abysmal, and going to the airport on weekends is pretty much gambling with your flight unless you leave *a lot* earlier.

Metrorail has potential. Combined with an improved bus network, a rail (or dedicated busway even!) to the beach, and the intercity Tri-Rail and Brightline trains, Miami does not **have** to be as car-dependent as it is right now.
