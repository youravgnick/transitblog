---
title: "Tampa's Streetcars"
date: 2022-06-28T23:39:45Z
cover: /img/streetcar.jpg
description: "A heritage line in an otherwise transit-lacking city."
author: "Nick"
---

The streetcar system in Tampa is an interesting one to say the least. Starting off, it’s not much of a *system* at all, as it only has one line. That line - named the TECO line after the **T**ampa **E**lectric **Co**mpany -  connects the historic Ybor City district with the more modern downtown Tampa.  

The TECO line operates 10 streetcars, including one restored Birney car, one replica open-air “Breezer” car, and eight replica Birney cars. I rode on one of the replica Birney cars, #436, during my time in Tampa, however I would like to ride on the restored original when I return. 

The TECO line streetcar may be limited, but ridership is high (for Florida standards), most likely owing to the historical significance and the fact that it is free to ride. Yes, this streetcar is free to ride as of 2018.

Due to its relative success, the city of Tampa has planned to extend and modernise the streetcar network, including four miles of new track, six new stations, and a replacement of the replica heritage streetcars with new modern streetcars like those used in Toronto, Portland, and Amsterdam. This proposal has not come without challenges, as even though the city has received $67 million dollars in funding so far, the Florida Supreme Court declared the one percent county sales tax that would help fund the project unconstitutional. A measure to replace the previous tax with a new one may be on the 2022 ballot.

As much as I love the heritage streetcar line that Tampa has, it in its current state is not much more than a tourist line, and perhaps one that few lucky locals get to use to commute downtown. I am looking forward to the expansion that is proposed by the city of Tampa as it would improve the city’s lacking transit network.
